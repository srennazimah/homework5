package com.company;

public class Message {
    private String message;
    private int second;
    public Message(String ms, int second){
        this.message=ms;
        this.second=second;
    }

    public String getMs() {
        return message;
    }

    public int getSecond() {
        return second;
    }
}
